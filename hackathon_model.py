# -*- coding: utf-8 -*-
"""

Hackathon AI 2019

Model

"""

from keras.models import Sequential
from keras.layers import Activation, TimeDistributed, Dense, RepeatVector, Dropout, recurrent

input_layers    = 2
output_layers   = 2
dropout_pct     = 0.3
hidden_size     = 500
max_input_len   = 60
num_chars       = 75

model = Sequential()
for l in range(input_layers):
    model.add(recurrent.LSTM(hidden_size,
              input_shape = (None, num_chars), 
              kernel_initializer = 'he_normal', 
              return_sequences = (l + 1 < input_layers)))
    model.add(Dropout(dropout_pct))

model.add(RepeatVector(max_input_len))

for _ in range(output_layers):
    model.add(recurrent.LSTM(hidden_size,
              return_sequences=True,
              kernel_initializer='he_normal'))
    model.add(Dropout(dropout_pct))

model.add(TimeDistributed(Dense(num_chars,
          kernel_initializer='he_normal')))

model.add(Activation('softmax'))

model.compile(loss = 'categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

